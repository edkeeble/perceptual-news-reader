class Article {
  String title;
  String content;
  int currentPage;
  ArrayList<String> pages;
  PImage image;
  
  Article (String title_, String content_, String image_url_) {
    this.title = title_;
    this.content = wordwrap(content_, width/7);
    this.divideIntoPages();
    this.currentPage=0;
    this.image = loadImage(image_url_);
  }
  
  void divideIntoPages(){
    pages = new ArrayList<String>();
    int linesPerPage = (height-40) / 24;
    String[] lines = this.content.split("\n");
    
    println ("Lines: "+lines.length);
    for (int page=0;page<=floor(lines.length/linesPerPage);page++){
      String pageText="";
      for (int l=page*linesPerPage;l<(page+1)*linesPerPage;l++){
        if (l<lines.length){
          pageText+=lines[l]+"\n";
        }
      }
      pages.add(pageText);
    }
  }
  
  String getCurrentPage(){
    return this.pages.get(this.currentPage);
  }
  
  boolean nextPage(){
    if (currentPage<pages.size()-1){
      currentPage++;
      return true;
    } else {
      return false;
    }  
  }
  
  boolean previousPage(){
    if (currentPage>0){
      currentPage--;
      return true;
    } else {
      return false;
    }
  }
    
}

String wordwrap(String basestring,int maxlen)
{
  String newstring="";
  String newline="";
  
  String[] lines = basestring.split("\n");
  
  for (int i=0;i<lines.length;i++){
    String s = lines[i];
  
    if (s.length() > maxlen){
      while (true){
        int marker = maxlen-1;
        while (!isspace(s.charAt(marker)) && !isnewline(s.charAt(marker))){
          marker-=1;
        }   
        //remove line from original string and add it to the new string
        newline=s.substring(0,marker)+"\n";
        newstring = newstring+newline;
        s = s.substring(marker+1);
        
        if (s.length()<=maxlen){
          break;
        }
      }
    }
    newstring = newstring + s +"\n";
  }
  
  return newstring;
}

boolean isspace(char c){
  return (c==' ');
}

boolean isnewline(char c){
  return (c=='\n');
}
