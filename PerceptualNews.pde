import intel.pcsdk.*;

PXCUPipeline session;

PImage displayLM;

String feedUrl = "http://feeds.theguardian.com/theguardian/uk/rss";
String feedName;

ArrayList<Article> articles;

int currentArticle = 0;

int MODE_NAVIGATION = 0;
int MODE_ARTICLE = 1;

int currentMode = MODE_NAVIGATION;

PXCMGesture.GeoNode primaryHandNode;
PVector primaryHandPos;
PXCMGesture.GeoNode secondaryHandNode;
PVector secondaryHandPos;

String[] commands = {"next", "previous", "read", "view", "exit", "back"};

void setup(){
  size(800,600);
  
  // Initialize Perceptual Computing Camera
  session = new PXCUPipeline(this);
  if (!session.Init(PXCUPipeline.GESTURE|PXCUPipeline.VOICE_RECOGNITION))
    exit();
    
  session.SetVoiceCommands(commands);
  
  int[] sizeLM = new int[2];
  session.QueryLabelMapSize(sizeLM);
  
  displayLM = createImage(sizeLM[0],sizeLM[1],RGB);
  
  articles = new ArrayList<Article>();
  processRssFeed(feedUrl);
  
  primaryHandNode = new PXCMGesture.GeoNode();
  primaryHandPos = new PVector();
  secondaryHandNode = new PXCMGesture.GeoNode();
  secondaryHandPos = new PVector();  
}

void draw(){  
  if (!session.AcquireFrame(false)) return; // could not acquire camera data
  
  session.QueryLabelMapAsImage(displayLM);
  PXCMGesture.Gesture gesture_data = new PXCMGesture.Gesture();
  if (session.QueryGesture(PXCMGesture.GeoNode.LABEL_ANY, gesture_data)){
    if (gesture_data.label == PXCMGesture.Gesture.LABEL_NAV_SWIPE_LEFT){
      navigateNext();
    } else if (gesture_data.label == PXCMGesture.Gesture.LABEL_NAV_SWIPE_RIGHT){
      navigatePrevious();
    } else if (gesture_data.label == PXCMGesture.Gesture.LABEL_NAV_SWIPE_DOWN){
      navigateEnter();
    } else if (gesture_data.label == PXCMGesture.Gesture.LABEL_NAV_SWIPE_UP){
      navigateExit();
    }
  }
  
  if (session.QueryGeoNode(PXCMGesture.GeoNode.LABEL_BODY_HAND_PRIMARY|PXCMGesture.GeoNode.LABEL_HAND_MIDDLE,primaryHandNode)){
    primaryHandPos = new PVector(primaryHandNode.positionImage.x,primaryHandNode.positionImage.y);
  }
  
  if (session.QueryGeoNode(PXCMGesture.GeoNode.LABEL_BODY_HAND_SECONDARY|PXCMGesture.GeoNode.LABEL_HAND_MIDDLE,secondaryHandNode)){
    secondaryHandPos = new PVector(secondaryHandNode.positionImage.x,secondaryHandNode.positionImage.y);
  }
  
  PXCMVoiceRecognition.Recognition voiceData = new PXCMVoiceRecognition.Recognition();
  if(session.QueryVoiceRecognized(voiceData)){
      if (voiceData.dictation.equals("next")){
        navigateNext();
      } else if (voiceData.dictation.equals("previous")){
        navigatePrevious();
      } else if (voiceData.dictation.equals("read") || voiceData.dictation.equals("view")){
        currentMode=MODE_ARTICLE;
      } else if (voiceData.dictation.equals("exit") || voiceData.dictation.equals("back")){
        currentMode=MODE_NAVIGATION;
      }
  }
  session.ReleaseFrame();
  
  
  background(0);
  
  if (currentMode==MODE_NAVIGATION){
    PImage displayImage = articles.get(currentArticle).image;
    tint(255,100);
    imageMode(CENTER);
    image(displayImage,width/2,height/2,(float)height/displayImage.height * displayImage.width, height);
    noTint();
    
    textSize(48);
    fill(255);
    text(articles.get(currentArticle).title,10,40,width-10,height-40);
  } else if (currentMode==MODE_ARTICLE){
    fill(255);
    textSize(18);
    text(articles.get(currentArticle).title,10,40);
    textSize(12);
    text(articles.get(currentArticle).getCurrentPage(),10,70,width-20,height-80);
  }
  
  textSize(18);
  fill(255);
  textAlign(LEFT,TOP);
  text(feedName,10,10); 
  
  fill(255,50);
  noStroke();
  ellipseMode(CENTER);
  ellipse(map(primaryHandPos.x,0,displayLM.width,width,0),map(primaryHandPos.y,0,displayLM.height,0,height),20,20);
  fill(0,125,255,50);
  ellipse(map(secondaryHandPos.x,0,displayLM.width,width,0),map(secondaryHandPos.y,0,displayLM.height,0,height),20,20);
  
}

void processRssFeed(String url) {
  XML xml = loadXML(url);
  XML channel = xml.getChild("channel");
  
  feedName = channel.getChild("title").getContent();
  
  XML[] children = channel.getChildren("item");
  for (int i=0;i<children.length;i++){
    XML title = children[i].getChild("title");
    XML content = children[i].getChild("description");
    XML[] images = children[i].getChildren("media:content");
    String imageUrl = "";
    for (int j=0;j<images.length;j++){
      if (images[j].getString("type").equals("image/jpeg")){
        imageUrl = images[j].getString("url");
        println("Image URL: "+imageUrl);
        break;
      }
    }
    
    String strippedContent = content.getContent().replaceAll("</p>","\n\n").replaceAll("\\<.*?\\>", "");
    
    Article article = new Article(title.getContent(),strippedContent,imageUrl);
    articles.add(article);
    println(article.title);
    println(article.content);
  }
}

void keyPressed(){
  if (keyCode==LEFT){
    navigatePrevious();
  } else if (keyCode==RIGHT) {
    navigateNext();
  } else if (keyCode==DOWN){
    navigateEnter();
  } else if (keyCode==UP){
    navigateExit();
  }   
}

void navigateNext(){
  if (currentMode==MODE_NAVIGATION){
    currentArticle++;
    if (currentArticle>=articles.size()){
      currentArticle=0;
    }
  } else if (currentMode==MODE_ARTICLE){
    articles.get(currentArticle).nextPage();
  }
}

void navigatePrevious(){
  if (currentMode==MODE_NAVIGATION){
    currentArticle--;
    if (currentArticle<0){
      currentArticle = articles.size()-1;
    }
  } else if (currentMode==MODE_ARTICLE){
    articles.get(currentArticle).previousPage();
  }
}

void navigateEnter(){
  currentMode = MODE_ARTICLE;
}

void navigateExit(){
  currentMode = MODE_NAVIGATION;
}


